<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once 'ConcreteFactory1.php';
require_once 'ConcreteFactory2.php';

$ConcreteFactory1 = new ConcreteFactory1();
$ConcreteFactory2 = new ConcreteFactory2();

$ProductA1 = $ConcreteFactory1->createProductA();
$ProductB1 = $ConcreteFactory1->createProductB();

$ProductA2 = $ConcreteFactory2->createProductA();
$ProductB2 = $ConcreteFactory2->createProductB();

echo '$ConcreteFactory1->createProductA(); ';
var_dump($ProductA1);
echo '<br>';

echo '$ConcreteFactory1->createProductB(); ';
var_dump($ProductB1);
echo '<br>';

echo '$ConcreteFactory2->createProductA(); ';
var_dump($ProductA2); 
echo '<br>';

echo '$ConcreteFactory2->createProductB(); ';
var_dump($ProductB2);
echo '<br>';
