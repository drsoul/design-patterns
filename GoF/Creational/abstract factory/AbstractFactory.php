<?php

interface AbstractFactory
{

	public function createProductA();
	public function createProductB();
}
