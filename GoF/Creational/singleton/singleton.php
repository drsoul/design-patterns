<?php

class Singleton
{
    
    private static $instance;
    
    private function __construct(){} #close instantiate via New	
	private function __clone(){} #close instantiate clone		
	private function __wakeup(){} #close instantiate unserialize	
    
    public static function getInstance()
    {
        if($this->instance == null){
            $this->instance = new self();
        }
        
        return $this->instance;
    }    
    
}
